import xlrd
import xlwt
import glob
import os

# Do you want the first two columns in the output file to have the original file name and sheet name?
include_original_workbook_sheet_columns = True

# Where are the xls files stored? We look for all xls and xlsx file types so store them in their own folder
path_to_excel_files = 'Michelles Lists/'

# Must end with .xls
output_file_name = 'output.xls'


################################

# Make sure the output file does not currently exist.
if os.path.isfile(output_file_name):
    os.remove(output_file_name)

# If we want the original workbook information, need to offset the original sheet columns by 2
counter_offset = 0
if include_original_workbook_sheet_columns:
    counter_offset = 2

writer = xlwt.Workbook()

for original_workbook_filepath in glob.glob(os.path.join(path_to_excel_files, '*.xls*')):

    wb = xlrd.open_workbook(original_workbook_filepath)

    for original_sheet in wb.sheets():

        # Don't bother with empty sheets.
        if not original_sheet.nrows:
            continue

        # Need to ensure we have unique original_sheet names, keep trying until
        #   we have a unique name using an incrementing value
        unique_sheet_num = 0
        new_sheet_name = original_sheet.name
        while True:

            try:
                worksheet = writer.add_sheet(new_sheet_name)
                break
            except Exception:
                unique_sheet_num += 1
                new_sheet_name = '{}_{}'.format(original_sheet.name, unique_sheet_num)

        print('Copying {} with {} rows'.format(original_sheet.name, original_sheet.nrows))

        columns = {}

        for row_number in range(original_sheet.nrows):

            if include_original_workbook_sheet_columns:

                # First 2 columns will be the original filename and sheet name, need to account for the header row
                if row_number:
                    worksheet.write(row_number, 0, original_workbook_filepath)
                    worksheet.write(row_number, 1, original_sheet.name)
                else:
                    worksheet.write(row_number, 0, 'Original Workbook')
                    worksheet.write(row_number, 1, 'Original Sheet Name')

            row = original_sheet.row(row_number)
            for column_number, cell in enumerate(row, start=counter_offset):
                cell = cell.value

                worksheet.write(row_number, column_number, cell)

if not output_file_name.endswith('.xls'):
    output_file_name = '{}.xls'.format(output_file_name.split('.', 1)[-1])
writer.save(output_file_name)
