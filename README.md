Excel File Combiner
===================

This script will allow you to supply it a folder that contains 
many *.xls files and will combine all workbooks and sheets within, 
into a single workbook.

Requirements
------------

* xlrd
* xlwt

Or you can...

``pip install -r requirements.txt``
 
Configuration
-------------

If you want the first 2 columns of each new sheet to contain the 
original workbooks filename and the original sheet name, set this 
to True (default). Otherwise set to default to straight copy sheets.

``include_original_workbook_sheet_columns = True``

Where are the xls files located?

```path_to_excel_files = 'C:/excel files to combine/'```

Where and what to call the output file?

``output_file_name = 'output.xls'``
